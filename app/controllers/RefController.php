<?php

class RefController extends Controller
{
    public function index() {
        Config::set('curPage', "ref");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'ref/index.php');
    }

    public function isAuthorized(){
        return true;
    }
}
<?php

/**
 * User controller
 *
 * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
 * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
 */

class UserController extends Controller{

    public function beforeAction(){

        parent::beforeAction();

        $action = $this->request->param('action');
        $actions = ['updateProfileInfo', 'updateProfilePicture', 'reportBug', 'processInstagramLink', 'createWithdrawal'];
        $this->Security->requirePost($actions);

        switch($action){
            case "updateProfileInfo":
                $this->Security->config("form", [ 'fields' => ['name', 'password', 'email', 'confirm_email']]);
                break;
            case "updateProfilePicture":
                $this->Security->config("form", [ 'fields' => ['file']]);
                break;
            case "reportBug":
                $this->Security->config("form", [ 'fields' => ['subject', 'label', 'message']]);
                break;
            case "processInstagramLink":
                $this->Security->config("form", [ 'fields' => ['username']]);
                break;
            case 'createWithdrawal':
                $this->Security->config("form", [ 'fields' => ['amount', 'phone']]);
                break;
        }
    }

    public function profile(){
        Config::setJsConfig('curPage', "profile");
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'user/profile.php');
    }

    public function updateProfileInfo(){

        $name           = $this->request->data("name");
        $password       = $this->request->data("password");
        $email          = $this->request->data("email");
        $confirmEmail   = $this->request->data("confirm_email");

        $result = $this->user->updateProfileInfo(Session::getUserId(), $name, $password, $email, $confirmEmail);

        if(!$result){

            Session::set('profile-info-errors', $this->user->errors());

        }else{

            $message  = "Ваш профиль был обновлен. ";
            $message .= $result["emailUpdated"]? "Пожалуйста, проверьте ваш новый адрес электронной почты, чтобы подтвердить изменения, или ваш текущий адрес электронной почты, чтобы отменить изменения": "";

            Session::set('profile-info-success', $message);
        }

        return $this->redirector->root("User/Profile");
    }

    public function updateProfilePicture(){

        $fileData   = $this->request->data("file");
        $image      = $this->user->updateProfilePicture(Session::getUserId(), $fileData);

        if(!$image){
            Session::set('profile-picture-errors', $this->user->errors());
        }

        return $this->redirector->root("User/Profile");
    }

    /**
     * revoke email updates
     *
     * You must be logged in with your current email
     */
    public function revokeEmail(){

        $userId  = $this->request->query("id");
        $userId  = empty($userId)? null: Encryption::decryptId($this->request->query("id"));
        $token   = $this->request->query("token");

        $result = $this->user->revokeEmail($userId, $token);

        if(!$result){
            return $this->error(404);
        }else{
            $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'user/profile.php', ["emailUpdates" => ["success" => "Your email updates has been revoked successfully."]]);
        }
    }

    /**
     * confirm on email updates
     *
     * You must be logged in with your current email
     */
    public function updateEmail(){

        $userId  = $this->request->query("id");
        $userId  = empty($userId)? null: Encryption::decryptId($this->request->query("id"));
        $token   = $this->request->query("token");

        $result = $this->user->updateEmail($userId, $token);
        $errors = $this->user->errors();

        if(!$result && empty($errors)){
            return $this->error(404);
        }else if(!$result && !empty($errors)){
            $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'user/profile.php', ["emailUpdates" => ["errors" => $this->user->errors()]]);
        }else{
            $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'user/profile.php',
                ["emailUpdates" => ["success" => "Ваши электронные обновления были успешно обновлены."]]);
        }
    }

    public function withdrawal()
    {
        Config::set('curPage', "withdrawal");
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'withdrawal/index.php');
    }

    public function newWithdrawal() {
        Config::set('curPage', "newWithdrawal");
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'withdrawal/new.php');
    }

    public function createWithdrawal() {
        $amount     = $this->request->data('amount');
        $phone      = $this->request->data('phone');

        $result = $this->user->withdrawal(Session::getUserId(), $amount, $phone);

        if (!$result) {
            Session::set('withdrawal-errors', $this->user->errors());

            return $this->redirector->root('user/newWithdrawal');
        }

        return $this->redirector->root('user/withdrawal');
    }

    public function instagramLink() {
        $user = $this->user->getProfileInfo(Session::getUserId());

        if (!is_null($user['instagram'])) {
            return $this->redirector->root("tasks");
        }
        
        Config::setJsConfig('curPage', "instagramLink");
        $code = substr(md5(uniqid(mt_rand(), true)) , 0, 8);
        Session::set('instagram-link-code', $code);

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'instagramLink/index.php', ['code' => $code]);
    }

    public function instagramUnlink()
    {
        $result = $this->user->instagramUnlink(Session::getUserId());

        if (!$result) {
            Session::set('instagram-link-errors', $this->user->errors());
        }

        return $this->redirector->root("User/InstagramLink");
    }


    public function processInstagramLink() {
        $username = $this->request->data("username");

        $code = Session::get('instagram-link-code');

        $result = $this->user->instagramLink(Session::getUserId(), $username, $code);

        if (!$result) {
            Session::set('instagram-link-errors', $this->user->errors());
            return $this->redirector->root("User/InstagramLink");
        }

        return $this->redirector->dashboard();
    }

    /**
     * users can report bugs, features, or enhancement
     * - Bug is an error you encountered
     * - Feature is a new functionality you suggest to add
     * - Enhancement is an existing feature, but you want to improve
     *
     */
    public function bugs(){
        Config::set('curPage', "bugs");
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'bugs/index.php');
    }

    /**
     * send email to admin for reporting any bugs, features, or enhancement
     *
     */
    public function reportBug(){

        $subject = $this->request->data("subject");
        $label   = $this->request->data("label");
        $message = $this->request->data("message");

        $result = $this->user->reportBug(Session::getUserId(), $subject, $label, $message);

        if(!$result){
            Session::set('report-bug-errors', $this->user->errors());
        }else{
            Session::set('report-bug-success', "E-mail было отправлено успешно, мы рассмотрим ваш отчет.");
        }
        
        return $this->redirector->root("User/Bugs");
    }

    public function isAuthorized(){
        return true;
    }
}

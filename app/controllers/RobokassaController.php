<?php

class RobokassaController extends Controller
{
    public function initialize() {
        $this->loadComponents([
            'Auth',
            'Security'
        ]);
    }

    public function callback() {
        $kassa = new Robokassa();

        $inv_id     = $this->request->query('InvId');
        $out_sum    = $this->request->query('OutSum');
        $action     = $this->request->query('shp_action');
        $user_id    = $this->request->query('shp_user_id');
        $task_id	= $this->request->query('shp_task_id');

        $kassa->inv_id = $inv_id;
        $kassa->out_sum = $out_sum;

        $custom_values = ['action' => $action];
        if ($action === 'pay_task' && $task_id !== null) {
            $custom_values['task_id'] = $task_id;
        } else if ($action === 'pay_pack' && $user_id !== null) {
            $custom_values['user_id'] = $user_id;
        }

        $kassa->addCustomValues($custom_values);

        if($kassa->checkHash($this->request->query('crc'))) {
            try {
                switch ($action) {
                    case 'pay_pack':
                        $user_id = Encryption::decryptId($user_id);

                        if ($out_sum >= 1000) {
                            $this->user->setStatus($user_id, 'premium');
                        } else if ($out_sum >= 500) {
                            $this->user->setStatus($user_id, 'standard');
                        }
                        break;
                    case 'pay_task':
                        $task_id = Encryption::decryptId($task_id);

                        if (!$this->task->exists($task_id)) {
                            throw new Exception("Task not found");
                        }

                        $task = $this->task->getById($task_id);
                        if ($out_sum >= ($task['amount'] * $task['count'])) {
                            $this->task->paid($task_id);
                        }
                        break;
                    default:
                        break;
                }

                echo "OK" . $inv_id;
                exit(0);
            } catch (Exception $e) {
                // Fail
            }
        }

        echo "FAIL";exit(0);
    }

    public function success() {
        $action = $this->request->query('shp_action');

        if ($action === 'pay_task') {
            return $this->redirector->root('tasks/me');
        }

        return $this->redirector->root('tasks');
    }

    public function failure() {
        $action = $this->request->query('shp_action');

        if ($action === 'pay_task') {
            return $this->redirector->root('tasks/me');
        }

        return $this->redirector->root('tasks');
    }
}
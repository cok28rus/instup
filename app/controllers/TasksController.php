<?php

class TasksController extends Controller
{
    public function beforeAction(){
        parent::beforeAction();

        $action  = $this->request->param('action');
        $actions = ['create'];
        $this->Security->requirePost($actions);

        switch($action){
            case "create":
                $this->Security->config("form", [ 'fields' => ['url', 'amount', 'count']]);
                break;
            case "go":
                $this->Security->config("validateCsrfToken", true);
                $this->Security->config("form", [ 'fields' => ['task_id']]);
                break;
            case "delete":
                $this->Security->config("validateCsrfToken", true);
                $this->Security->config("form", [ 'fields' => ['task_id']]);
                break;
        }

        if (!is_null(Session::getUserId())) {
            $this->user->checkLimits(Session::getUserId());

            $user = $this->user->getProfileInfo(Session::getUserId());
            $limitTime = (strtotime($user['limit_expended_at']) + 604800) - time();

            Config::setJsConfig('limitTime', $limitTime);
        }
    }

    /**
     * show posts page
     *
     */
    public function index(){
        Config::set('curPage', "tasks");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'tasks/index.php');
    }

    public function me() {
        Config::set('curPage', "tasks_me");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'tasks/me.php');
    }

    public function add() {
        Config::set('curPage', "tasks_add");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'tasks/add.php');
    }

    public function delete($taskId = 0) {
        $taskId = Encryption::decryptId($taskId);
        if(!$this->task->exists($taskId)){
            return $this->error(404);
        }

        $this->task->deleteById($taskId);

        return $this->redirector->root("tasks/me");
    }

    public function create() {
        $url        = $this->request->data("url");
        $amount     = $this->request->data("amount");
        $count      = $this->request->data("count");

        $result = $this->task->create(Session::getUserId(), $url, $amount, $count);

        if(!$result){
            Session::set('tasks-errors', $this->task->errors());
            return $this->redirector->root("tasks/add");
        }

        return $this->redirector->root("tasks/me");
    }

    public function go($taskId = 0) {
        $taskId = Encryption::decryptId($taskId);

        if(!$this->task->exists($taskId)){
            return $this->error(404);
        }

        $result = $this->task->go(Session::getUserId(), $taskId);

        if (!$result) {
            Session::set("task-go-errors-" . $taskId, $this->task->errors());
        }

        return $this->redirector->root("tasks");
    }

    public function isAuthorized(){
        return true;
    }
}
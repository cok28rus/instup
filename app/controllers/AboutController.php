<?php

/**
 * Created by PhpStorm.
 * User: Ilchenko Mikhail
 * Date: 22.12.2016
 * Time: 5:34
 */
class AboutController extends Controller
{
    public function index(){

        Config::set('curPage', "about");
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'about/index.php');
    }

    public function isAuthorized(){
        return true;
    }
}
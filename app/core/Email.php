<?php

 /**
  * Email Class
  *
  * Sending emails via SMTP.
  * It uses PHPMailer library to send emails.
  *
  * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
  * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
  */

 class Email{

     /**
      * This is the constructor for Email object.
      *
      * @access private
      */
     private function __construct(){}

     /**
      * send an email
      *
      * @access public
      * @static static method
      * @param  string  $type Email constant - check config.php
      * @param  string  $email
      * @param  array   $userData
      * @param  array   $data any associated data with the email
      * @throws Exception If failed to send the email
      */
     public static function sendEmail($type, $email, $userData, $data){

         $mail             = new PHPMailer();
         $mail->IsSMTP();

         // good for debugging, otherwise keep it commented
         // $mail->SMTPDebug  = EMAIL_SMTP_DEBUG;
         $mail->SMTPAuth   = Config::get('EMAIL_SMTP_AUTH');
         $mail->SMTPSecure = Config::get('EMAIL_SMTP_SECURE');
         $mail->Host       = Config::get('EMAIL_SMTP_HOST');
         $mail->Port       = Config::get('EMAIL_SMTP_PORT');
         $mail->Username   = Config::get('EMAIL_SMTP_USERNAME');
         $mail->Password   = Config::get('EMAIL_SMTP_PASSWORD');

         $mail->CharSet = 'UTF-8';
         $mail->isHTML(true);

         $mail->SetFrom(Config::get('EMAIL_FROM'), Config::get('EMAIL_FROM_NAME'));
         $mail->AddReplyTo(Config::get('EMAIL_REPLY_TO'));

         switch($type){
             case (Config::get('EMAIL_EMAIL_VERIFICATION')):
                 $mail->Body = self::getEmailVerificationBody($userData, $data);
                 $mail->Subject    = Config::get('EMAIL_EMAIL_VERIFICATION_SUBJECT');
                 $mail->AddAddress($email);
                 break;
             case (Config::get('EMAIL_REVOKE_EMAIL')):
                 $mail->Body = self::getRevokeEmailBody($userData, $data);
                 $mail->Subject    = Config::get('EMAIL_REVOKE_EMAIL_SUBJECT');
                 $mail->AddAddress($email);
                 break;
             case (Config::get('EMAIL_UPDATE_EMAIL')):
                 $mail->Body = self::getUpdateEmailBody($userData, $data);
                 $mail->Subject    = Config::get('EMAIL_UPDATE_EMAIL_SUBJECT');
                 $mail->AddAddress($email);
                 break;
             case (Config::get('EMAIL_PASSWORD_RESET')):
                 $mail->Body = self::getPasswordResetBody($userData, $data);
                 $mail->Subject    = Config::get('EMAIL_PASSWORD_RESET_SUBJECT');
                 $mail->AddAddress($email);
                 break;
             case (Config::get('EMAIL_REPORT_BUG')):
                 $mail->Body = self::getReportBugBody($userData, $data);
                 $mail->Subject    = "[".ucfirst($data["label"])."] " . Config::get('EMAIL_REPORT_BUG_SUBJECT') . " | " . $data["subject"];
                 $mail->AddAddress($email);
                 break;
         }

         // If you don't have an email setup, you can instead save emails in log.txt file using Logger.
         // Logger::log("EMAIL", $mail->Body);
         if(!$mail->Send()) {
             throw new Exception("Email couldn't be sent to ". $userData["id"] ." for type: ". $type);
         }
     }

     /**
      * Construct the body of Password Reset email
      *
      * @access private
      * @static static method
      * @param  array   $userData
      * @param  array   $data
      * @return string  The body of the email.
      */
     private static function getPasswordResetBody($userData, $data){

         $body = "";
         $body .= "Уважаемый " . $userData["name"] . ", \n\nВы можете восстановить свой пароль по следующей ссылке: ";
         $body .= Config::get('EMAIL_PASSWORD_RESET_URL') . "?id=" . urlencode(Encryption::encryptId($userData["id"])) . "&token=" . urlencode($data["password_token"]);
         $body .= "\n\nЕсли вы не просили восстановить свой пароль, пожалуйста, свяжитесь с администратором напрямую.";
         $body .= "\n\nС уважением\nInstUP Team";

         return $body;
     }

     /**
      * Construct the body of Email Verification email
      *
      * @access private
      * @static static method
      * @param  array   $userData
      * @param  array   $data
      * @return string  The body of the email.
      *
      */
     private static function getEmailVerificationBody($userData, $data){

         $body  = "";
         $body .= "Уважаемый " . $userData["name"] . ", \n\nПожалуйста, потдвержите свою электронную почту по следующей ссылке: ";
         $body .= "<a href=\"" . Config::get('EMAIL_EMAIL_VERIFICATION_URL') . "?id=" . urlencode(Encryption::encryptId($userData["id"])) . "&token=" . urlencode($data["email_token"]) . "\">" . Config::get('EMAIL_EMAIL_VERIFICATION_URL') . "?id=" . urlencode(Encryption::encryptId($userData["id"])) . "&token=" . urlencode($data["email_token"]) . "</a>";
         $body .= "\n\nЕсли вы не добавляли свою электронную почту, пожалуйста, свяжитесь с администратором напрямую.";
         $body .= "\n\nС уважением\nInstUP Team";

         return $body;
     }

     /**
      * Construct the body of Revoke Email Changes email
      *
      * @access private
      * @static static method
      * @param  array   $userData
      * @param  array   $data
      * @return string  The body of the email.
      *
      */
     private static function getRevokeEmailBody($userData, $data){

         $body  = "";
         $body .= "С уважением " . $userData["name"] . ", \n\nВаш электронный адрес был изменен, Вы можете отменить изменения по следующей ссылке: ";
         $body .= "<a href=\"" . Config::get('EMAIL_REVOKE_EMAIL_URL') . "?id=" . urlencode(Encryption::encryptId($userData["id"])) . "&token=" . urlencode($data["email_token"]) . "\">". Config::get('EMAIL_REVOKE_EMAIL_URL') . "?id=" . urlencode(Encryption::encryptId($userData["id"])) . "&token=" . urlencode($data["email_token"]) ."</a>";
         $body .= "\n\nЕсли вы не обновляли вашу электронную почту, пожалуйста, свяжитесь с администратором напрямую.";
         $body .= "\n\nС уважением\nInstUP PHP Team";

         return $body;
     }

     /**
      * Construct the body of Update Email email
      *
      * @access private
      * @static static method
      * @param  array   $userData
      * @param  array   $data
      * @return string  The body of the email.
      *
      */
     private static function getUpdateEmailBody($userData, $data){

         $body  = "";
         $body .= "Уважаемый " . $userData["name"] . ", \n\nПожалуйста, подтвердите свое новое электронное письмо по следующей ссылке: ";
         $body .= Config::get('EMAIL_UPDATE_EMAIL_URL') . "?id=" . urlencode(Encryption::encryptId($userData["id"])) . "&token=" . urlencode($data["pending_email_token"]);
         $body .= "\n\nЕсли вы понятия не имеете, что это за письмо, вы можете его игнорировать.";
         $body .= "\n\nС уважением\nInstUP Team";

         return $body;
     }

     /**
      * Construct the body of Report Bug, Feature or Enhancement email
      *
      * @access private
      * @static static method
      * @param  array   $userData
      * @param  array   $data
      * @return string  The body of the email.
      *
      */
     private static function getReportBugBody($userData, $data){
         $body  = "";
         $body .= "Пользователь: " . $userData["name"] . ", \n\n" . $data["message"];
         $body .= "\n\n\nFrom: " . $userData["id"] . " | " . $userData["name"];
         $body .= "\n\nС уважением\nInstUP PHP Team";

         return $body;
     }

 }
	

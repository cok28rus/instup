<?php

/**
 * Class Robokassa
 */
class Robokassa {
    protected $login;
    protected $password1;
    protected $password2;
    protected $endpoint = 'https://merchant.roboxchange.com/Index.aspx?';
    protected $customVars = [];

    public $out_sum;
    public $email = false;
    public $inv_id = 0;
    public $desc;
    public $inc_curr_label = '';
    public $culture = 'ru';


    /**
     * Robokassa constructor.
     *
     * @param bool $test
     */
    public function __construct()
    {
        $this->login        = Config::get('ROBOKASSA_LOGIN');
        $this->password1    = Config::get('ROBOKASSA_PASSWORD1');
        $this->password2    = Config::get('ROBOKASSA_PASSWORD2');

        if (true === Config::get('ROBOKASSA_TEST')) {
            $this->endpoint .= "isTest=1&";
        }
    }

    /**
     * @param $vars
     * @throws Exception
     */
    public function addCustomValues($vars)
    {
        if (!is_array($vars)) {
            throw new Exception('Function `addCustomValues` take only array`s');
        }

        foreach($vars as $key => $value) {
            $this->customVars["shp_" . $key] = $value;
        }
    }

    /**
     * @return string
     */
    public function getRedirectURL()
    {
        $customVars     = $this->getCustomValues();

        $hash           = "{$this->login}";
        $hash          .= ":{$this->out_sum}";
        $hash          .= ":{$this->inv_id}";
        $hash          .= ":{$this->password1}";
        $hash          .= "{$customVars}";

        $hash           = md5($hash);

        $invId          = ($this->inv_id !== '') ? '&InvId=' . $this->inv_id : '';
        $IncCurrLabel   = ($this->inc_curr_label !== '') ? '&IncCurrLabel=' . $this->inc_curr_label : '';
        $Email          = ($this->email !== '') ? '&Email=' . $this->email : '';

        return $this->endpoint . 'MrchLogin=' . $this->login
        . '&OutSum=' . (float) $this->out_sum
        . $invId
        . '&InvDesc=' . urlencode($this->desc)
        . '&SignatureValue=' . $hash
        . $IncCurrLabel
        . $Email
        . '&Culture=' . $this->culture
        . $this->getCustomValues($url = true);
    }

    /**
     * @param $hash
     * @param bool $checkSuccess
     * @return bool
     */
    public function checkHash($hash, $checkSuccess = false)
    {
        $customVars     = $this->getCustomValues();
        $password       = $checkSuccess ? $this->password1 :$this->password2;
        $hashGenerated  = md5("{$this->out_sum}:{$this->inv_id}:{$password}{$customVars}");

        return (strtolower($hash) == $hashGenerated);
    }

    /**
     * @param $hash
     * @return bool
     */
    public function checkSuccess($hash) {
        return $this->checkHash($hash, true);
    }

    /**
     * @param bool $url
     * @return string
     */
    protected function getCustomValues($url = false)
    {
        $out = '';
        $customVars = array();
        if (!empty($this->customVars)) {
            foreach($this->customVars as $key => $value) {
                $customVars[$key] = $key . '=' . $value;
            }

            sort($customVars);

            if($url === true) {
                $out = '&' . join('&', $customVars);
            } else {
                $out = ':' . join(':', $customVars);
            }
        }

        return $out;
    }
}
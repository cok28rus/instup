<?php

class Withdrawal
{
    public function getAll() {
        $database   = Database::openConnection();
        $query  = "SELECT * ";
        $query .= "FROM withdrawals ";
        $query .= "ORDER BY date DESC ";

        $database->prepare($query);
        $database->execute();

        return $database->fetchAllAssociative();
    }

    public function getById($withdrawalId){
        $database = Database::openConnection();
        $query  = "SELECT * ";
        $query .= "FROM withdrawals ";
        $query .= "WHERE id = :id LIMIT 1 ";

        $database->prepare($query);
        $database->bindValue(':id', $withdrawalId);
        $database->execute();

        return $database->fetchAssociative();
    }

    public function findByUserId($userId) {
        $database = Database::openConnection();
        $query  = "SELECT * ";
        $query .= "FROM withdrawals ";
        $query .= "WHERE user_id = :user_id ";
        $query .= "ORDER BY date DESC ";

        $database->prepare($query);
        $database->bindValue(':user_id', $userId);
        $database->execute();

        return $database->fetchAllAssociative();
    }
}
<?php

 /**
  * User Class
  *
  * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
  * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
  */

class User extends Model{

    /**
      * Table name for this & extending classes.
      *
      * @var string
      */
    public $table = "users";

    /**
     * returns an associative array holds the user info(image, name, id, ...etc.)
     *
     * @access public
     * @param  integer $userId
     * @return array Associative array of current user info/data.
     * @throws Exception if $userId is invalid.
     */
    public function getProfileInfo($userId){

        $database = Database::openConnection();
        $database->getById("users", $userId);

        if($database->countRows() !== 1){
            throw new Exception("User ID " .  $userId . " doesn't exists");
        }

        $user = $database->fetchAssociative();

        $user["id"]    = (int)$user["id"];
        $user["image"] = PUBLIC_ROOT . "img/profile_pictures/" . $user['profile_picture'];
        // $user["email"] = empty($user['is_email_activated'])? null: $user['email'];

        return $user;
      }

    /**
     * Update the current profile
     *
     * @access public
     * @param  integer $userId
     * @param  string  $name
     * @param  string  $password
     * @param  string  $email
     * @param  string  $confirmEmail
     * @return bool|array
     * @throws Exception If profile couldn't be updated
     *
     */
    public function updateProfileInfo($userId, $name, $password, $email, $confirmEmail){

        $database = Database::openConnection();
        $curUser = $this->getProfileInfo($userId);

        $name   = (!empty($name) && $name !== $curUser["name"])? $name: null;
        $email  = (!empty($confirmEmail) || (!empty($email) && $email !== $curUser["email"]))? $email: null;

        // if new email === old email, this shouldn't return any errors for email,
        // because they are not 'required', same for name.
        $validation = new Validation();
        if(!$validation->validate([
            "Name" => [$name, "alphaNumWithSpaces|minLen(4)|maxLen(30)"],
            "Password" => [$password, "minLen(6)"],
            "Email" => [$email, "email|emailUnique|maxLen(50)|equals(".$confirmEmail.")"]])){
            $this->errors = $validation->errors();
            return false;
        }

        $profileUpdated = ($password || $name || $email)? true: false;
        if($profileUpdated) {

            $options = [
                $name     => "name = :name ",
                $password => "hashed_password = :hashed_password ",
                $email    => "pending_email = :pending_email, pending_email_token = :pending_email_token, email_token = :email_token "
            ];

            $database->beginTransaction();
            $query   = "UPDATE users SET ";
            $query  .= $this->applyOptions($options, ", ");
            $query  .= "WHERE id = :id LIMIT 1 ";
            $database->prepare($query);

            if($name) {
                $database->bindValue(':name', $name);
            }
            if($password) {
                $database->bindValue(':hashed_password', password_hash($password, PASSWORD_DEFAULT, array('cost' => Config::get('HASH_COST_FACTOR'))));
            }
            if($email) {
                $emailToken = sha1(uniqid(mt_rand(), true));
                $pendingEmailToken = sha1(uniqid(mt_rand(), true));
                $database->bindValue(':pending_email', $email);
                $database->bindValue(':pending_email_token', $pendingEmailToken);
                $database->bindValue(':email_token', $emailToken);
            }

            $database->bindValue(':id', $userId);
            $result = $database->execute();

            if(!$result){
                $database->rollBack();
                throw new Exception("Couldn't update profile");
            }

            // If email was updated, then send two emails,
            // one for the current one asking user optionally to revoke,
            // and another one for the new email asking user to confirm changes.
            if($email){
                $name = ($name)? $name: $curUser["name"];
                Email::sendEmail(Config::get('EMAIL_REVOKE_EMAIL'), $curUser["email"], ["name" => $name, "id" => $curUser["id"]], ["email_token" => $emailToken]);
                Email::sendEmail(Config::get('EMAIL_UPDATE_EMAIL'), $email, ["name" => $name, "id" => $curUser["id"]], ["pending_email_token" => $pendingEmailToken]);
            }

            $database->commit();
        }

        return ["emailUpdated" => (($email)? true: false)];
    }

    public function instagramLink($userId, $username, $code) {
        $database = Database::openConnection();
        $curUser = $this->getProfileInfo($userId);

        $validation = new Validation();
        if(!$validation->validate([
            "Username" => [$username, "required"]])){
            $this->errors = $validation->errors();
            return false;
        }

        if (!$instUser = $this->getInstagramUser($username)) {
            $this->errors[] = "Пользователь не найден.";
            return false;
        }

        if ($this->instagram_exists($username)) {
            $this->errors[] = "Instagram аккаунт занят другим пользователем.";
            return false;
        }

        if (strpos($instUser['biography'], $code) === false) {
            $this->errors[] = "Код не найден на Вашем аккаунте.";
            return false;
        }

        $query = "UPDATE users SET instagram = :instagram WHERE id = :id LIMIT 1";

        $database->prepare($query);
        $database->bindValue(':instagram', $username);
        $database->bindValue(':id', $userId);
        $result = $database->execute();

        if(!$result){
            throw new Exception("Неудалось привязать instagram: " . $username);
        }

        $user = $this->getProfileInfo($userId);

        return $user;
    }

    public function instagramUnlink($userId) {
        $database = Database::openConnection();
        $user = $this->getProfileInfo($userId);

        if (is_null($user['instagram'])) {
            $this->errors[] = "у Вас не привязан аккаунт Instagram.";
            return false;
        }

        $query = "UPDATE users SET instagram = NULL WHERE id = :id";

        $database->prepare($query);
        $database->bindValue(':id', $userId);
        $result = $database->execute();

        if(!$result){
            throw new Exception("Неудалось отвязать instagram: ");
        }

        return true;
    }

    protected function instagram_exists($username) {
        $database = Database::openConnection();

        $query = "SELECT COUNT(*) as count FROM users WHERE instagram = :instagram";
        $database->prepare($query);
        $database->bindValue(':instagram', $username);
        $database->execute();

        if ($database->fetchAssociative()['count'] > 0) {
            return true;
        }

        return false;
    }

    protected function getInstagramUser($username) {
        $client = new \GuzzleHttp\Client();

        try {
            $url = sprintf('https://www.instagram.com/%s/?__a=1', $username);
            $response = $client->get($url);

            return json_decode((string) $response->getBody(), true)['user'];
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return false;
        }
    }

    /**
     * Update Profile Picture.
     *
     * @access public
     * @param  integer $userId
     * @param  array   $fileData
     * @return mixed
     * @throws Exception If failed to update profile picture.
     */
    public function updateProfilePicture($userId, $fileData){

        $image = Uploader::uploadPicture($fileData, $userId);

        if(!$image) {
            $this->errors = Uploader::errors();
            return false;
        }

        $database = Database::openConnection();
        $query  =  "UPDATE users SET profile_picture = :profile_picture WHERE id = :id LIMIT 1";

        $database->prepare($query);
        $database->bindValue(':profile_picture', $image["basename"]);
        $database->bindValue(':id', $userId);
        $result = $database->execute();

        // if update failed, then delete the user picture
        if(!$result){
            Uploader::deleteFile(IMAGES . "profile_pictures/" . $image["basename"]);
            throw new Exception("Profile Picture ". $image["basename"] . " couldn't be updated");
        }

        return $image;
      }

    public function setStatus($userId, $status = 'standard'){
        $database = Database::openConnection();
        $query = "UPDATE users ";
        $query.= "SET status = :status, count_completed_tasks = 0, limit_expended_at = NULL ";
        $query.= "WHERE id = :id LIMIT 1 ";
        $database->prepare($query);

        switch ($status) {
            case 'premium':
                $database->bindValue(':status', 'premium');
                break;
            case 'standard':
                $database->bindValue(':status', 'standard');
                break;
            default:
                throw new Exception("Неправильный статус для Users " . $userId);
                break;
        }

        $database->bindValue(':id', $userId);
        $result = $database->execute();

        if (!$result) {
            throw new Exception("Неудалось обновить статус аккаунта " . $userId);
        }
    }

    /**
     * revoke Email updates
     *
     * @access public
     * @param  integer  $userId
     * @param  string   $emailToken
     * @return mixed
     * @throws Exception If failed to revoke email updates.
     */
    public function revokeEmail($userId, $emailToken){

        if (empty($userId) || empty($emailToken)) {
            return false;
        }

        $database = Database::openConnection();
        $database->prepare("SELECT * FROM users WHERE id = :id AND email_token = :email_token AND is_email_activated = 1 LIMIT 1");
        $database->bindValue(':id', $userId);
        $database->bindValue(':email_token', $emailToken);
        $database->execute();
        $users = $database->countRows();

        $query = "UPDATE users SET email_token = NULL, pending_email = NULL, pending_email_token = NULL WHERE id = :id LIMIT 1";
        $database->prepare($query);
        $database->bindValue(':id', $userId);
        $result = $database->execute();

        if(!$result){
            throw new Exception("Couldn't revoke email updates");
        }

        if ($users === 1){
            return true;
        }else{
            Logger::log("REVOKE EMAIL", "User ID ". $userId . " is trying to revoke email using wrong token " . $emailToken, __FILE__, __LINE__);
            return false;
        }
    }

    /**
     * update Email
     *
     * @access public
     * @param  integer  $userId
     * @param  string   $emailToken
     * @return mixed
     * @throws Exception If failed to update current email.
     */
    public function updateEmail($userId, $emailToken){

        if (empty($userId) || empty($emailToken)) {
            return false;
        }

        $database = Database::openConnection();
        $database->prepare("SELECT * FROM users WHERE id = :id AND pending_email_token = :pending_email_token AND is_email_activated = 1 LIMIT 1");
        $database->bindValue(':id', $userId);
        $database->bindValue(':pending_email_token', $emailToken);
        $database->execute();

        if($database->countRows() === 1){

            $user = $database->fetchAssociative();
            $validation = new Validation();
            $validation->addRuleMessage("emailUnique", "We can't change your email because it has been already taken!");

            if(!$validation->validate(["Email" => [$user["pending_email"], "emailUnique"]])){

                $query = "UPDATE users SET email_token = NULL, pending_email = NULL, pending_email_token = NULL WHERE id = :id LIMIT 1";
                $database->prepare($query);
                $database->bindValue(':id', $userId);
                $database->execute();

                $this->errors = $validation->errors();

                return false;

            }else{

                $query = "UPDATE users SET email = :email, email_token = NULL, pending_email = NULL, pending_email_token = NULL WHERE id = :id LIMIT 1";
                $database->prepare($query);
                $database->bindValue(':id', $userId);
                $database->bindValue(':email', $user["pending_email"]);
                $result = $database->execute();

                if(!$result){
                    throw new Exception("Couldn't update current email");
                }

                return true;
            }
        }else {

            $query = "UPDATE users SET email_token = NULL, pending_email = NULL, pending_email_token = NULL WHERE id = :id LIMIT 1";
            $database->prepare($query);
            $database->bindValue(':id', $userId);
            $database->execute();

            Logger::log("UPDATE EMAIL", "User ID ". $userId . " is trying to update email using wrong token " . $emailToken, __FILE__, __LINE__);
            return false;
        }

    }

    public function checkLimits($userId) {
        $user = $this->getProfileInfo($userId);

        # Аккаунт еще не ограничен
        if (is_null($user['limit_expended_at'])) {
            switch ($user['status']) {
                case 'basic':
                    if ($user['count_completed_tasks'] >= 25) {
                        $this->setLimitExpandedAt($userId);
                    }
                    break;
                case 'standard':
                    if ($user['count_completed_tasks'] >= 200) {
                        $this->setLimitExpandedAt($userId);
                    }
                    break;
                case 'premium':
                    if ($user['count_completed_tasks'] >= 500) {
                        $this->setLimitExpandedAt($userId);
                    }
                    break;
            }
        } else {
            if ((strtotime($user['limit_expended_at']) + 604800) <= time()) {
                $this->setLimitExpandedAt($userId, true);
                return;
            }

            switch ($user['status']) {
                case 'basic':
                    if ($user['count_completed_tasks'] < 25) {
                        $this->setLimitExpandedAt($userId, true);
                    }
                    break;
                case 'standard':
                    if ($user['count_completed_tasks'] < 200) {
                        $this->setLimitExpandedAt($userId, true);
                    }
                    break;
                case 'premium':
                    if ($user['count_completed_tasks'] < 500) {
                        $this->setLimitExpandedAt($userId, true);
                    }
                    break;
            }
        }
    }

    public function getCountRefs($userId) {
        $database = Database::openConnection();
        $database->prepare("SELECT * FROM users WHERE ref_id = :user_id");
        $database->bindValue(':user_id', $userId);
        $database->execute();

        return $database->countRows();
    }

    private function setLimitExpandedAt($userId, $new = false) {
        $database = Database::openConnection();
        $query = "UPDATE users ";

        if ($new) {
            $query .= "SET count_completed_tasks = 0, limit_expended_at = NULL ";
        } else {
            $query .= "SET limit_expended_at = now() ";
        }

        $query .= "WHERE id = :id LIMIT 1 ";
        $database->prepare($query);
        $database->bindValue(':id', $userId);
        $result = $database->execute();

        if(!$result){
            throw new Exception("");
        }
    }

    /**
     * Reporting Bug, Feature, or Enhancement.
     *
     * @access public
     * @param  integer $userId
     * @param  string  $subject
     * @param  string  $label
     * @param  string  $message
     * @return bool
     *
     */
    public function reportBug($userId, $subject, $label, $message){

        $validation = new Validation();
        if(!$validation->validate([
            "Subject" => [$subject, "required|minLen(4)|maxLen(80)"],
            "Label" => [$label, "required|inArray(".Utility::commas(["bug", "feature", "enhancement"]).")"],
            "Message" => [$message, "required|minLen(4)|maxLen(1800)"]])){

            $this->errors = $validation->errors();
            return false;
          }

        $curUser = $this->getProfileInfo($userId);
        $data = ["subject" => $subject, "label" => $label, "message" => $message];

        // email will be sent to the admin
        Email::sendEmail(Config::get('EMAIL_REPORT_BUG'), Config::get('ADMIN_EMAIL'), ["id" => $userId, "name" => $curUser["name"]], $data);

        return true;
      }

    public function withdrawal($userId, $amount, $phone) {
        $validation = new Validation();
        if(!$validation->validate([
            "Amount" => [$amount, "required|integer|minNum(500))"],
            "Phone" => [$phone, "required|phone"]])){

            $this->errors = $validation->errors();
            return false;
        }

        $user = (new User())->getProfileInfo($userId);
        if ($user['balance'] < $amount) {
            $this->errors[] = "Недостаточно средств";
            return false;
        }

        $database = Database::openConnection();

        $database->beginTransaction();
        $query = "INSERT INTO withdrawals (`user_id`,`amount`,`phone`) VALUES (:user_id, :amount, :phone);";
        $database->prepare($query);
        $database->bindValue(":user_id", $userId);
        $database->bindValue(":amount", $amount);
        $database->bindValue(":phone", $phone);

        $result = $database->execute();

        if (!$result) {
            $database->rollBack();
            throw new Exception("Withdrawal create error");
        }

        $query = "UPDATE users ";
        $query .= "SET balance = balance - :amount ";
        $query .= "WHERE id = :user_id LIMIT 1";
        $database->prepare($query);
        $database->bindValue(":amount", $amount);
        $database->bindValue(":user_id", $userId);

        $result = $database->execute();

        if (!$result) {
            $database->rollBack();
            throw new Exception("Withdrawal update user balance error");
        }

        $database->commit();

        return true;
    }
  }
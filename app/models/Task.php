<?php
/**
 * Created by PhpStorm.
 * User: Ilchenko Mikhail
 * Date: 20.12.2016
 * Time: 23:57
 */
class Task extends Model
{
    /**
     * get all photos
     *
     * @param int $pageNum
     * @return array
     */
    public function getAll($pageNum = 1){

        $pagination = Pagination::pagination("tasks", "", [], $pageNum);
        $offset     = $pagination->getOffset();
        $limit      = $pagination->perPage;

        $database   = Database::openConnection();
        $query  = "SELECT * ";
        $query .= "FROM tasks ";
        $query .= "LIMIT $limit OFFSET $offset";

        $database->prepare($query);
        $database->execute();
        $photos = $database->fetchAllAssociative();

        return array("tasks" => $photos, "pagination" => $pagination);
    }

    /**
     * get photo by Id.
     *
     * @param $photoId
     * @return array|bool
     */
    public function getById($photoId){
        $database = Database::openConnection();
        $query  = "SELECT * ";
        $query .= "FROM tasks ";
        $query .= "WHERE id = :id LIMIT 1 ";

        $database->prepare($query);
        $database->bindValue(':id', $photoId);
        $database->execute();

        return $database->fetchAssociative();
    }

    /**
     * @param $userId
     * @param $url
     * @param $amount
     * @param $count
     * @return bool
     * @throws Exception
     */
    public function create($userId, $url, $amount, $count) {

        $validation = new Validation();
        if(!$validation->validate([
            'Url'       => [$url, "required"],
            'Amount'    => [$amount, "required|integer|minNum(3)|maxNum(8)"],
            'Count'     => [$count, "required|integer|minNum(10)|maxNum(9999)"]])) {
            $this->errors = $validation->errors();
            return false;
        }

        $url = preg_replace("/\s*\/\?taken-by=[a-z\d-_\.]{1,255}\s*$/i", "", $url);

        $image = $this->getImageUrlFromInstagram($url);

        if (!$image) {
            $this->errors[] = "Не удалось получить фотографию с инстаграма, проверьте введеный URL.";
            return false;
        }

        $database = Database::openConnection();
        $query  =  "INSERT INTO tasks (`url`, `user_id`, `image`, `amount`, `count`) VALUES (:url, :user_id, :image, :amount, :count)";
        $database->prepare($query);
        $database->bindValue(':url',        $url);
        $database->bindValue(':user_id',    $userId);
        $database->bindValue(':image',      $image);
        $database->bindValue(':amount',     $amount);
        $database->bindValue(':count',      $count);
        $result = $database->execute();

        if(!$result){
            throw new Exception("Ошибка при создании фото.");
        }

        return true;
    }

    public function paid($taskId) {
        $database = Database::openConnection();
        $query = "UPDATE tasks ";
        $query.= "SET paid = 1 ";
        $query.= "WHERE id = :id LIMIT 1 ";
        $database->prepare($query);
        $database->bindValue(':id', $taskId);
        $result = $database->execute();

        if (!$result) {
            throw new Exception("Неудалось оплатить задание " . $taskId);
        }
    }

    /**
     * @param $userId
     * @param $taskId
     * @return bool
     * @throws Exception
     */
    public function go($userId, $taskId)
    {
        if ($this->hasCompliedTask($userId, $taskId)) {
            $this->errors[] = "Нельзя повторно выполнять задание";
            return false;
        }

        if (!is_null($this->getLimitExpendedAtByUserId($userId))) {
            $this->errors[] = "Вы исчерпали лимит";
            return false;
        }

        $task = $this->getById($taskId);

        if ($task['paid'] === 0) {
            $this->errors[] = "Задание не оплачено";
            return false;
        }

        $database = Database::openConnection();
        $database->beginTransaction();

        $query = "INSERT INTO completed_tasks (`user_id`, `task_id`) VALUES (:user_id, :task_id)";
        $database->prepare($query);
        $database->bindValue(':user_id', $userId);
        $database->bindValue(':task_id', $taskId);

        if(!$database->execute()){
            throw new Exception("Неудалось создать строку в таблице completed_tasks: " . $taskId);
        }

        $query = "UPDATE users SET balance = balance + :balance WHERE id = :user_id";
        $database->prepare($query);
        $database->bindValue(":balance", $task['amount']);
        $database->bindValue(":user_id", $userId);

        if (!$database->execute()) {
            $database->rollBack();
            throw new Exception("Неудалось обновить баланс пользователю " . $userId);
        }

        $database->commit();
    }

    protected function hasCompliedTask($userId, $taskId) {
        $database = Database::openConnection();
        $database->prepare("SELECT * FROM completed_tasks WHERE user_id = :user_id AND task_id = :task_id LIMIT 1");
        $database->bindValue(':user_id', $userId);
        $database->bindValue(':task_id', $taskId);
        $database->execute();

        return $database->countRows() === 1;
    }

    public function getLimitExpendedAtByUserId($userId) {
        $database = Database::openConnection();
        $database->prepare("SELECT * FROM users WHERE id = :user_id");
        $database->bindValue(':user_id', $userId);
        $database->execute();

        $result = $database->fetchAssociative();

        return $result['limit_expended_at'];
    }

    public function getCountCompletedTasksByTaskId($taskId) {
        $database = Database::openConnection();
        $database->prepare("SELECT * FROM completed_tasks WHERE task_id = :task_id");
        $database->bindValue(':task_id', $taskId);
        $database->execute();

        return $database->countRows();
    }

    public function findAvailableTasksForUserId($userId) {
        $database = Database::openConnection();
        $query  = "SELECT t.* FROM tasks as t ";
        $query .= "LEFT JOIN completed_tasks as ct ";
        $query .= "ON ct.task_id = t.id AND ct.user_id = :user_id ";
        $query .= "WHERE ct.id IS NULL ";
        $query .= "AND t.user_id <> :author_id ";
        $query .= "AND t.paid = 1";
        $database->prepare($query);
        $database->bindValue(':user_id', $userId);
        $database->bindValue(':author_id', $userId);
        $database->execute();

        $tasks = $database->fetchAllAssociative();

        return $tasks;
    }

    public function findTasksByUserId($userId) {
        $database = Database::openConnection();
        $query  = "SELECT * FROM tasks ";
        $query .= "WHERE user_id = :user_id ";
        $query .= "ORDER BY date DESC ";
        $database->prepare($query);
        $database->bindValue(':user_id', $userId);
        $database->execute();

        $tasks = $database->fetchAllAssociative();

        return $tasks;
    }

    protected function getImageUrlFromInstagram($url) {
        if (!preg_match("/\s*instagram\.com\/p\/[a-z\d-_]{1,255}[\/]{0,1}\s*$/i", $url)) {
            return false;
        }

        $client = new \GuzzleHttp\Client();

        try {
            $url .= "?__a=1";
            $response = $client->get($url);

            return json_decode((string) $response->getBody(), true)['media']['display_src'];
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return false;
        }
    }
}
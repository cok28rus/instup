<?php
$status = "";
$countTasks = 0;
$countCompletedTasks = $info['count_completed_tasks'];

switch ($info['status']) {
    case 'basic':
        $countTasks = 25;
        $status = "Базовый";
        $countAvailableTasks = ($countTasks - $countCompletedTasks);
        break;
    case 'standard':
        $countTasks = 200;
        $status = "Стандарт";
        $countAvailableTasks = ($countTasks - $countCompletedTasks);
        break;
    case 'premium':
        $countTasks = 500;
        $status = "Премиум";
        $countAvailableTasks = ($countTasks - $countCompletedTasks);
        break;
}
?>
<div class="col-md-9">
    <div class="profile-content">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Зарабатывай на лайках</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <?php if (is_null($info['instagram'])) { ?>
                    <div class="alert alert-success">
                        Перед тем как начать, перейдите по <a href="<?= PUBLIC_ROOT ?>about">ссылке</a> и ознакомьтесь с принципами нашей деятельности.
                    </div>

                    <div class='warning'>
                        <div class='alert alert-warning'>
                            <i class='fa fa-angle-right'></i>
                            <b>Внимание!</b> У Вас не привязана учетная запись <b>Instagram</b>. <a href="<?= PUBLIC_ROOT; ?>User/instagramLink">Перейдите по ссылке</a> чтобы привязать учетную запись <b>Instagram</b> и продолжить работу.
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-warning">
                        <i class='fa fa-angle-right'></i>
                        <b>Внимание!</b> Ваш аккаунт имеет статус <b>«<?= $status ?>»</b>, поэтому Вам доступно всего <b><?= $countTasks ?></b> заданий в неделю. <br/>
                        <i class='fa fa-angle-right'></i>
                        Осталось доступных заданий: <b><?= $countAvailableTasks ?> шт.</b>
                    </div>

                    <?php if (!is_null($info['limit_expended_at'])) {  ?>
                        <br/>
                        <div class='alert alert-info'>
                            <i class='fa fa-angle-right'></i>
                            Вы исчерпали весь лимит заданий, следующие <b><?= $countTasks ?></b> заданий Вы получите через <span id="limit-timer"></span>.
                            <?php if (in_array($info['status'], ['basic', 'standard'])) { ?>
                                <br/>
                                <i class='fa fa-angle-right'></i>
                                Чтобы увеличить лимит, измените статус аккаунта на <b>«Стандарт»</b> или <b>«Премиум»</b>.
                            <?php } ?>
                        </div>
                        
                        <?php
                        $kassa          = new Robokassa();
                        $kassa->email   = $info['email'];
                        $kassa->addCustomValues([
                            "action"    => "pay_pack",
                            "user_id"   => Encryption::encryptId(Session::getUserId())
                        ]);
                        ?>

                        <?php if (in_array($info['status'], ['basic', 'standard'])) {  ?>
                            <div class="col-md-11 col-centered">
                                <div class="pricing">
                                    <ul>
                                        <li class="unit price-primary">
                                            <div class="price-title">
                                                <h3>₽0</h3>
                                            </div>
                                            <div class="price-body">
                                                <h4>Базовый</h4>
                                                <p>Долго, но без затрат</p>
                                                <ul>
                                                    <li>25 заданий в неделю</li>
                                                </ul>
                                            </div>
                                            <div class="price-foot">
                                                <button type="button" class="btn btn-primary"><?= ($info['status'] === 'basic') ? "Ваш статус": "Недоступен" ?></button>
                                            </div>
                                        </li>
                                        <li class="unit price-success active">
                                            <div class="price-title">
                                                <h3>₽1000</h3>
                                            </div>
                                            <div class="price-body">
                                                <h4>Премиум</h4>
                                                <p>Максимально быстрый</p>
                                                <ul>
                                                    <li></li>
                                                    <li>500 заданий в неделю</li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="price-foot">
                                                <?php
                                                $kassa->out_sum = 1000;
                                                $kassa->desc = "Приобретение пакета \"Премиум\".";

                                                switch ($info['status']) {
                                                    case 'basic':
                                                    case 'standard':
                                                        echo "<a href=\"". $kassa->getRedirectURL() ."\" type=\"button\" class=\"btn btn-success\">Установить</a>";
                                                        break;
                                                }
                                                ?>
                                            </div>
                                        </li>
                                        <li class="unit price-warning">
                                            <div class="price-title">
                                                <h3>₽500</h3>
                                            </div>
                                            <div class="price-body">
                                                <h4>Стандарт</h4>
                                                <p>Быстрый заработок</p>
                                                <ul>
                                                    <li></li>
                                                    <li>200 заданий в неделю</li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="price-foot">
                                                <?php
                                                $kassa->out_sum = 500;
                                                $kassa->desc = "Приобретение пакета \"Стандарт\".";

                                                switch ($info['status']) {
                                                    case 'basic':
                                                        echo "<a href=\"". $kassa->getRedirectURL() ."\" type=\"button\" class=\"btn btn-warning\">Установить</a>";
                                                        break;
                                                    case 'standard':
                                                        echo "<button type=\"button\" class=\"btn btn-warning\">Ваш статус</button>";
                                                        break;
                                                }
                                                ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>

                        <br/>
                    <?php } ?>

                    <div class="col-md-9 col-lg-9 col-centered">
                        <div class="tasks">
                            <?php $tasks = $this->controller->task->findAvailableTasksForUserId($info['id']); ?>
                            <?php
                                foreach ($tasks as $key => $task) {
                                    if ($task['amount'] > 5 && $info['status'] === 'basic' && is_null($info['limit_expended_at'])) {
                                        unset($tasks[$key]);
                                        continue;
                                    }

                                    ?>
                                    <div class="task clearfix">
                                        <div class="image">
                                            <a href="<?= $task["url"] ?>" target="_blank">
                                                <img src="<?= $task["image"] ?>" height="32" width="32" />
                                            </a>
                                        </div>
                                        <div class="title">Нажать «мне нравится»</div>
                                        <div class="amount">+ <?= $task["amount"] ?> руб.</div>
                                        <div class="actions">
                                            <form action="<?= PUBLIC_ROOT . "tasks/go/" . urlencode(Encryption::encryptId($task["id"])); ?>" method="GET">
                                                <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                                <button type="submit" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-heart"></i> Мне нравится
                                                </button>
                                            </form>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <?php
                                    if(!empty(Session::get("task-go-errors-" . $task['id']))){
                                        foreach (Session::getAndDestroy("task-go-errors-" . $task['id']) as $error) { ?>
                                            <div class="error error_msg task_error"><?= $error ?></div>
                                            <?php
                                        }
                                    } ?>
                            <?php } ?>

                            <?php if (empty($tasks)) { ?>
                                <div class="tasks_empty">Нет доступных заданий.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

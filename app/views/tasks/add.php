
<div class="col-md-9">
    <div class="profile-content">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Создать задание</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-10 col-lg-10 col-centered">
                <form action="<?php echo PUBLIC_ROOT; ?>tasks/create" method="post">
                    <div class="form-group col-lg-12">
                        <label for="">Ссылка: </label>
                        <input class="form-control" name="url" type="text" placeholder="https://instagram.com/..." required=""
                               data-toggle="popover" data-html="true" data-trigger="focus" data-placement="auto" data-content="Вы можете указать ссылку на фото или видео.">
                    </div>

                    <div class="form-group col-lg-7">
                        <label for="">Стоимость выполнения: </label>
                        <input class="form-control" id="add_task_amount" name="amount" type="text" placeholder="руб." required=""
                               data-toggle="popover" data-html="true" data-trigger="focus" data-placement="auto" data-content="За выполнение Вашего задания, пользователь получит на счет указанную сумму рублей.<br><br>Чем выше стоимость - тем выше скорость.<br><br> Доступные значения – от <b>3</b> до <b>8</b> рублей.">
                    </div>

                    <div class="form-group col-lg-5">
                        <label for="">Количество: </label>
                        <input class="form-control" id="add_task_count" name="count" type="text" placeholder="отметок «Мне нравится»" required=""
                               data-toggle="popover" data-html="true" data-trigger="focus" data-placement="auto" data-content="Укажите, скольким людям нужно выполнить Ваше задание. <br><br> Доступные значения - от <b>10</b> до <b>9999</b> отметок.">
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                    </div>
                    <div class="form-group form-actions col-lg-12">
                        <button type="submit" value="submit" class="btn btn-md btn-primary">
                            <i class="fa fa-pencil"></i> Создать задание
                        </button><div id="add_task_points_result">— <b>0</b> рублей</div>
                    </div>
                </form>

                <div class="clearfix"></div>

                <?php
                if(!empty(Session::get('tasks-errors'))){
                    echo $this->renderErrors(Session::getAndDestroy('tasks-errors'));
                }
                ?>
            </div>
        </div>
    </div>
</div>
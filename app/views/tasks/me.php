
<div class="col-md-9">
    <div class="profile-content">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Ваши задания</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <?php $tasks = $this->controller->task->findTasksByUserId(Session::getUserId()); ?>
                <div class="col-md-10 col-lg-10 col-centered">
                    <div class="tasks">
                        <div class="header">
                            <span class="count pull-left">Вы создали <?= $this->plural_form(count($tasks), array('задание','задания','заданий')) ?></span>
                            <a href="<?= PUBLIC_ROOT ?>tasks/add" class="pull-right" >Создать</a>
                            <div class="clearfix"></div>
                        </div>
                        <?php if (count($tasks) > 0) { ?>
                            <?php foreach ($tasks as $task) { ?>
                                <div class="task clearfix">
                                    <div class="image">
                                        <a href="<?= $task["url"] ?>" target="_blank">
                                            <img src="<?= $task["image"] ?>" height="32" width="32" />
                                        </a>
                                    </div>
                                    <div class="title">
                                        <?= ($task['paid'] === 0) ? "Оплатите задание": "Выполняется..." ?>
                                    </div>
                                    <div class="count"><?= $this->controller->task->getCountCompletedTasksByTaskId($task['id']) ?> из <b><?= $task['count'] ?></b></div>
                                    <div class="amount">+ <?= $task["amount"] ?> руб.</div>
                                    <div class="actions">
                                        <?php if ($task['paid'] === 1) { ?>
                                            <a href="<?= PUBLIC_ROOT . "tasks/delete/" . urlencode(Encryption::encryptId($task["id"])) . "?csrf_token=" . Session::generateCsrfToken(); ?>" class="btn btn-sm btn-danger">
                                                <i class="fa fa-remove"></i> Удалить
                                            </a>
                                        <?php } else { ?>
                                            <?php
                                            $kassa = new Robokassa();
                                            $kassa->out_sum = ($task['amount'] * $task['count']);
                                            $kassa->desc = "Оплатить продвижение фотографии в Instagram";
                                            $kassa->addCustomValues([
                                                'action'    => 'pay_task',
                                                'task_id'   => Encryption::encryptId($task['id']),
                                            ]);
                                            ?>
                                            <a href="<?= $kassa->getRedirectURL() ?>" class="btn btn-sm btn-info">
                                                <i class="fa fa-money"></i> Оплатить
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="tasks_empty">Вы не создали ни одного задания.</div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

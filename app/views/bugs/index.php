<div class="col-md-9">
    <div class="profile-content">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Сообщить об ошибке</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <div class="row">
                    <div class="col-lg-12">

                        <?php if(empty(Session::get('report-bug-success'))){ ?>
                            <form action="<?php echo PUBLIC_ROOT; ?>User/reportBug" id="form-bug" method="post">
                                <div class="form-group">
                                    <label>Тема <span class="text-danger">*</span></label>
                                    <input type="text" name="subject" class="form-control" required maxlength="80" placeholder="Введите тему">
                                </div>

                                <div class="form-group">
                                    <label>Ошибка, особенность или идея? <span class="text-danger">*</span></label>
                                    <select name="label" class="form-control" size="1">
                                        <option value="bug">Ошибка</option>
                                        <option value="feature">Особенность</option>
                                        <option value="enhancement">Идея</option>
                                    </select>
                                    <p class="help-block">Ошибка с которой Вы столкнулись.</p>
                                    <p class="help-block">Особенностью является новая функциональность которую Вы предлагаете, чтобы добавить.</p>
                                    <p class="help-block">Идея является существующей особенностью, но вы хотите ее улучшить.</p>
                                </div>

                                <div class="form-group">
                                    <label>Сообщение <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="message" required rows="20" maxlength="1800"></textarea>
                                    <p class="help-block"><em>Максимальное количество символов <strong>1800</strong></em></p>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                </div>
                                <div class="form-group form-actions text-right">
                                    <button type="submit" name="submit" value="submit" class="btn btn-md btn-success">
                                        <i class="fa fa-check"></i> Отправить
                                    </button>
                                </div>
                            </form>
                        <?php } else { echo $this->renderSuccess(Session::getAndDestroy('report-bug-success')); } ?>
                        <?php
                        if(!empty(Session::get('report-bug-errors'))){
                            echo $this->renderErrors(Session::getAndDestroy('report-bug-errors'));
                        }
                        ?>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- END Newsfeed Block -->
        </div>
        <!-- /.row -->
    </div>
</div>
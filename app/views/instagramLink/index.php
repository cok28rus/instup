<div class="col-md-9">
    <div class="profile-content">
        <h4 class="page-header">Привязать Instagram</h4>

        <div class="row">
            <div class="col-lg-12">
                <form action="<?php echo PUBLIC_ROOT; ?>User/processInstagramLink" method="post" >
                    <div class="form-group">
                        <ol>
                            <li>Скопируйте код: <b><?= $code ?></b></li>
                            <li>Откройте свой инстаграм-профиль, нажмите "редактировать профиль"</li>
                            <li>В строке "о себе" вставьте код, сохраните (<a href="#" role="button" data-toggle="modal" data-target="#statModal">Посмотреть пример</a>)</li>
                            <li>В поле "инстаграм аккаунт" введите никнейм инстаграм-аккаунта</li>
                            <li>Нажмите на кнопку "Проверить код"</li>
                            <li>После нажатия кнопки код можно удалить</li>
                        </ol>
                    </div>
                    <div class="form-group col-lg-12">
                        <label>Инстаграм аккаунт</label>
                        <input dir="auto" type="text" name="username" class="form-control" required maxlength="30" placeholder="Введите ник вашего аккаунта в Instagram">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                    </div>
                    <div class="form-group col-lg-12 form-actions text-right">
                        <button type="submit" name="submit" value="submit" class="btn btn-md btn-primary">
                            <i class="fa fa-check"></i> Проверить код
                        </button>
                    </div>
                </form>

                <?php
                if(!empty(Session::get('instagram-link-errors'))){
                    echo $this->renderErrors(Session::getAndDestroy('instagram-link-errors'));
                }
                ?>
            </div>
            <!-- /.col-lg-6 (nested) -->
        </div>
    </div>
</div>

<div class="modal fade" id="statModal" tabindex="-1" role="dialog" aria-labelledby="statModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="statModalLabel">Пример добавления кода</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <div class="col-md-6"><img src="<?= PUBLIC_ROOT ?>img/other/20.png" class="img-responsive center-block" border="1" ></div>
                            <div class="col-md-6"><img src="<?= PUBLIC_ROOT ?>img/other/21.png" class="img-responsive center-block" border="1" ></div>
                        </div>
                        <div class="panel-footer text-center"><label>Пример добавления кода</label></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="profile-content">
        <h4 class="page-header">Вывод средств</h4>

        <div class="alert alert-warning">
            Обработка вывода обычно осуществляется в течении часа.
            <br>В некоторых случаях платеж может
            быть обработан до 3-х рабочих дней.
        </div>

        <div class="col-lg-12 text-right">
            <a href="<?php echo PUBLIC_ROOT; ?>user/newWithdrawal" class="btn btn-primary btn-sm">Создать</a>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Номер кошелька</th>
                    <th>Сумма</th>
                    <th>Дата и время</th>
                </tr>
            </thead>

            <tbody>
                <?php $withdrawals = $this->controller->withdrawal->findByUserId(Session::getUserId()); ?>
                <?php foreach ($withdrawals as $withdrawal) { ?>
                    <tr>
                        <td>#<?= $withdrawal['id'] ?></td>
                        <td><?= $withdrawal['phone'] ?></td>
                        <td><?= $withdrawal['amount'] ?></td>
                        <td><?= $withdrawal['date'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
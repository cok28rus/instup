<div class="col-md-9">
    <div class="profile-content">
        <h4 class="page-header">Вывод средств</h4>

        <div class="alert alert-warning">
            Обработка вывода обычно осуществляется в течении часа.
            <br>В некоторых случаях платеж может
            быть обработан до 3-х рабочих дней.
        </div>

        <div class="alert alert-warning">
            Вывод средств осуществляется только на кошельки <b>QIWI Wallet</b>.
        </div>

        <div class="col-lg-10 col-centered">
            <div class="row">
                <form action="<?php echo PUBLIC_ROOT; ?>user/createWithdrawal" method="POST">
                    <div class="form-group col-lg-4">
                        <label for="amount">Сумма вывода: </label>
                        <input type="number" id="amount" name="amount" class="form-control" placeholder="500" autofocus>
                    </div>
                    <div class="form-group col-lg-8">
                        <label for="phone">Номер кошелька: </label>
                        <input type="text" id="phone" name="phone" class="form-control" placeholder="+79876543210">
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                    </div>
                    <div class="form-group form-actions col-lg-12">
                        <button type="submit" value="submit" class="btn btn-md btn-primary">
                            <i class="fa fa-pencil"></i> Вывести
                        </button>
                    </div>
                </form>

                <div class="clearfix"></div>

                <?php
                if(!empty(Session::get('withdrawal-errors'))){
                    echo $this->renderErrors(Session::getAndDestroy('withdrawal-errors'));
                }
                ?>
            </div>
        </div>
    </div>
</div>
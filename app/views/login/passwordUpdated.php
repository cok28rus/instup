

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Изменение пароля</h3>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-success">
							<i class="fa fa-check-circle"></i> <strong>Поздравляем!</strong> Ваш пароль был успешно изменен.
								Пожалуйста <a href="<?= PUBLIC_ROOT; ?>">авторизуйтесь</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


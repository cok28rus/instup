
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Inst UP - заработай на лайках</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= PUBLIC_ROOT;?>css/bootstrap.min.css" rel="stylesheet">

    <link href="<?= PUBLIC_ROOT;?>css/bootflat.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="<?= PUBLIC_ROOT;?>css/timeTo.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= PUBLIC_ROOT;?>css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Assign CSRF Token to JS variable -->
    <?php Config::setJsConfig('csrfToken', Session::generateCsrfToken()); ?>
    <!-- Assign all configration variables -->
    <script>config = <?= json_encode(Config::getJsConfig()); ?>;</script>
</head>

<body>

<div class="container">
    <div class="row profile">
		<?php require_once(Config::get('VIEWS_PATH') . "layout/default/navigation.php");?>
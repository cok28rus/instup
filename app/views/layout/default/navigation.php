<?php
	$info = $this->controller->user->getProfileInfo(Session::getUserId());
?>

<div class="col-md-3">
    <div class="profile-sidebar">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
            <img src="<?= $info["image"] ?>" class="img-responsive" alt="">
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name">
                <?= $info['name'] ?>
            </div>
            <div class="profile-usertitle-job">
                Баланс: <b><?= $info["balance"] ?></b> руб.
            </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
            <a href="<?= PUBLIC_ROOT . "User/Profile" ?>" type="button" class="btn btn-success btn-sm">Профиль</a>
            <a href="<?= PUBLIC_ROOT . "Login/Logout" ?>" type="button" class="btn btn-danger btn-sm">Выйти</a>
        </div>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->

        <div class="profile-usermenu">
            <ul class="nav">
                <li <?= (in_array(Config::get('curPage'), ["tasks"])) ? 'class="active"': '' ?>>
                    <a href="<?= PUBLIC_ROOT . "tasks";?>">
                        <i class="fa fa-money" style="width: 15px;text-align: center;"></i>
                        Заработать
                    </a>
                </li>
                <li <?= (in_array(Config::get('curPage'), ["tasks_me", "tasks_add"])) ? 'class="active"': '' ?>>
                    <a href="<?= PUBLIC_ROOT . "tasks/me";?>">
                        <i class="fa fa-tasks" style="width: 15px;text-align: center;"></i>
                        Мои задания
                    </a>
                </li>
                <li <?= (in_array(Config::get('curPage'), ["withdrawal"])) ? 'class="active"': '' ?>>
                    <a href="<?= PUBLIC_ROOT . "user/withdrawal";?>">
                        <i class="fa fa-minus" style="width: 15px;text-align: center;"></i>
                        Вывод средств
                    </a>
                </li>
                <li <?= (in_array(Config::get('curPage'), ["about"])) ? 'class="active"': '' ?>>
                    <a href="<?= PUBLIC_ROOT . "about";?>">
                        <i class="fa fa-info" style="width: 15px;text-align: center;"></i>
                        О нас
                    </a>
                </li>
                <li <?= (in_array(Config::get('curPage'), ["ref"])) ? 'class="active"': '' ?>>
                    <a href="<?= PUBLIC_ROOT . "ref";?>">
                        <i class="fa fa-group" style="width: 15px;text-align: center;"></i>
                        Реферальная система
                    </a>
                </li>
                <li <?= (in_array(Config::get('curPage'), ["bugs"])) ? 'class="active"': '' ?>>
                    <a href="<?= PUBLIC_ROOT . "user/bugs";?>">
                        <i class="fa fa-envelope" style="width: 15px;text-align: center;"></i>
                        Сообщить об ошибке
                    </a>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
</div>

	
	<div class='error'>
		<div class='alert alert-danger'>
			<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
		<?php if(empty($errors)) { ?>
			<i class='fa fa-angle-right'></i>
			К сожалению! Был ошибка, Пожалуйста, повторите попытку позже или сообщить об ошибке
		<?php } else{ ?>
			<?php foreach((array)$errors as $error){?>
				<p><i class='fa fa-angle-right'></i> <?= $error; ?></p>
			<?php } ?>
		<?php } ?>
		</div>
	</div>

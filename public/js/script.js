(function ( $ ) {
    'use strict';

    // функция склонения числительных
    function declOfNum(number, titles) {
        var cases = [2, 0, 1, 1, 1, 2];
        return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
    }

    $(document).ready(function() {
        // popover
        $('[data-toggle="popover"]').popover();

        if (typeof config['limitTime'] != 'undefined') {
            if ($('#limit-timer').length > 0) {
                $('#limit-timer').timeTo({
                    seconds: config['limitTime'],
                    displayDays: 2,
                    theme: "white",
                    displayCaptions: false,
                    fontSize: 14,
                    callback: function () {
                        Location.reload();
                    }
                });
            }
        }

        // определение цены
        $('#add_task_amount, #add_task_count').keyup(function() {
            if (!$(this).val().match(/^([0-9]+)$/)) {
                $(this).val('');
            }

            var amount = $('#add_task_amount').val() * 1;
            var count = $('#add_task_count').val() * 1;
            var points = amount * count;

            if (points >=1) {
                $('#add_task_points_result').html('— <b>' + points + '</b> ' + declOfNum(points, ['рубль', 'рубля', 'рублей']));
            } else {
                $('#add_task_points_result').html('— <b>0</b> рублей');
            }
        });

        $('form').bind('submit', function() {
            $(this).find('button[type="submit"].btn-primary i').attr('class', 'fa fa-spinner fa-spin');
        });
    });
})( jQuery );
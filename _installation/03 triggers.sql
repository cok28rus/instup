CREATE TRIGGER `completed_tasks` BEFORE INSERT ON `completed_tasks`
 FOR EACH ROW UPDATE `users`
    SET count_completed_tasks = count_completed_tasks + 1
    WHERE users.id = NEW.user_id
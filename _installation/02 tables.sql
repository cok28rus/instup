CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `session_id` varchar(48) DEFAULT NULL,
 `cookie_token` varchar(128) DEFAULT NULL,
 `name` varchar(48) NOT NULL,
 `role` varchar(16) NOT NULL DEFAULT 'user',
 `instagram` varchar(30) NULL,
 `status` varchar(30) NOT NULL DEFAULT 'basic',
 `hashed_password` varchar(128) NOT NULL,
 `email` varchar(64) NOT NULL,
 `ref_id` int NULL,
 `balance` int NOT NULL DEFAULT '0',
 `count_completed_tasks` int NOT NULL DEFAULT '0',
 `limit_expended_at` datetime NULL,
 `is_email_activated` tinyint(1) NOT NULL DEFAULT '0',
 `email_token` varchar(48) DEFAULT NULL,
 `email_last_verification` int(11) DEFAULT NULL COMMENT 'unix timestamp',
 `pending_email` varchar(64) DEFAULT NULL COMMENT 'temporary email that will be used when user updates his current one',
 `pending_email_token` varchar(48) DEFAULT NULL,
 `profile_picture` varchar(48) NOT NULL DEFAULT 'default.png' COMMENT 'The base name for the image. Its not always unique because of default.jpg',
 PRIMARY KEY (`id`),
 UNIQUE KEY `email` (`email`),
 FOREIGN KEY (`ref_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `failed_logins` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_email` varchar(64) NOT NULL COMMENT 'It doesnt reference email in table users, this will prevent even unregistered users as well',
 `last_failed_login` int(11) DEFAULT NULL COMMENT 'unix timestamp of last failed login',
 `failed_login_attempts` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `ip_failed_logins` (
 `ip` varchar(48) NOT NULL,
 `user_email` varchar(64) NOT NULL COMMENT 'It doesnt reference email in table users',
 PRIMARY KEY (`ip`,`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `blocked_ips` (
 `ip` varchar(48) NOT NULL,
 PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `forgotten_passwords` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `password_token` varchar(48) DEFAULT NULL,
 `password_last_reset` int(11) DEFAULT NULL COMMENT 'unix timestamp of last password reset request',
 `forgotten_password_attempts` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 UNIQUE KEY `forgotten_passwords_user` (`user_id`),
 FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `tasks` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `url` VARCHAR(255) NOT NULL,
 `user_id` int(11) NOT NULL,
 `image` VARCHAR (255) NOT NULL,
 `count` int NOT NULL,
 `amount` int NOT NULL,
 `paid` int NOT NULL DEFAULT '0',
 `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `completed_tasks` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `task_id` int(11) NOT NULL,
 `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
 FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `withdrawals` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `amount` int(11) NOT NULL,
 `phone` VARCHAR(30) NOT NULL,
 `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;